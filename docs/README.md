---
home: true
heroImage: /aice_backdrop.jpg
tagline: Artificial Intelligence & Cognitive Engineering
actionText: Go to Docs →
actionLink: /guide/
features:
- title: Codebase
  details: The work listed here are free and open-sourced. Use it for your learning and share with others. Strictly non-commercial use only. 
- title: DL Framework
  details: Synapse is a framwork depeloped in python to easily design and experiment with DL models
- title: DL Solutions
  details: Custom designed DL model, optimized for custom user dataset to achieve highest performance
footer: Made by Syed Hasibur Rahman with ❤️
meta:
  - name: description
    content: Documentation for AICE Codebase
  - name: keywords
    content: AICE aice synapse
---
